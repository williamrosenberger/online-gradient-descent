
from setuptools import setup, find_packages

requires = [
    'pandas==0.23.4',
    'mat4py==0.4.2',
    'matplotlib==3.0.2'
]

setup(
    name='portfolio-management',
    version='0.0.1',
    author='William Rosenberger',
    description="Online gradient descent",
    long_description='Implemenation of online gradient descent for portfolio management.',
    long_description_content_type="text/markdown",
    packages=['manage_portfolio'],
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    install_requires=requires,
    entry_points={
        'console_scripts': [
            "manage = manage_portfolio.main:main"
        ]
    }
)
