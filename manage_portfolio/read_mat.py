
import pandas as pd
from mat4py import loadmat


def get_data(file):
    """
        Reads in the data as a Pandas dataframe.
    """
    raw = loadmat(file)
    data_as_dict = {name[0]: values for name, values in zip(raw['B'], raw['A'])}
    df = pd.DataFrame.from_dict(data_as_dict)

    return df
