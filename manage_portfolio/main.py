
import os

import pandas as pd
from matplotlib import pyplot as plt


DATA_PATH = os.path.abspath(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        '..',
        'data'
    )
)

ETA = .0014  # Step size
ROUNDOFF = 1e-6  # Acceptable roundoff error when running data checks.


def get_splits(ratios):
    """
        Finds a good way to split money for each time step
    """
    splits = [
        ratios.iloc[0] / ratios.iloc[0].sum()  # Start out by splitting the money based on which stock improved the most.
    ]

    for time, ratio in ratios.iterrows():
        gradient = splits[-1] / splits[-1].dot(ratio)

        not_norm = splits[-1] - ETA * gradient
        normed = not_norm / not_norm.sum()

        splits.append(normed.rename(time + 1))

        assert abs(1 - normed.sum()) <= ROUNDOFF, 'Most recent timestep does not sum to 1. It sums to {}. History of splits:\n{}'.format(
            splits[-1].sum(),
            pd.DataFrame(data=splits)
        )

    return pd.DataFrame(data=splits)


def final_profit(ratios, splits):
    """
        Builds a series containing the relative change in wealth from the beginning of investment.
    """
    gains = [
        1  # You start out with 100% of your money.
    ]
    for (_, this_ratio), (_, this_split) in zip(ratios.iterrows(), splits.iterrows()):
        gains.append(gains[-1] * this_ratio.dot(this_split))

    return pd.Series(data=gains).rename('My gains')


def get_overall_rise(df):
    """
        Returns a series containing the relative change in worth of the _entire_ stock market.
    """
    all_stocks = df.sum(axis='columns')

    return (all_stocks / all_stocks.shift(-1)).cumprod().rename('S&P Overall')


def main():
    df = pd.read_csv(os.path.join(DATA_PATH, 'data.csv'), index_col=0)

    # Get the ratios of todays' price divided by yesterdays' price.
    today = df
    yesterday = df.shift(-1)

    ratios = df / df.shift(-1)
    ratios = ratios[:-1]  # Drop the last row; it just contains NaNs

    splits = get_splits(ratios)
    profit = final_profit(ratios, splits)

    data_overview = pd.concat([profit, get_overall_rise(df)], axis=1)

    ax = data_overview.plot(kind='line')
    ax.set_xlabel('Time')
    ax.set_ylabel('Relative increase')

    print(data_overview)
    plt.savefig(os.path.join(DATA_PATH, 'out.png'))


if __name__ == '__main__':
    main()
